# Welcome to Markdown

Lab-44 exercice:

[This document](https://christophelarsonneur.gitlab.io/markdown-demo/LAB-4/) is fully readable from [gitlab](https://gitlab.com/ChristopheLarsonneur/markdown-demo/-/blob/main/docs/LAB-4.md).

The goal is to reproduce the [gitlab rendered version (with TOC)](https://gitlab.com/ChristopheLarsonneur/markdown-demo/-/blob/main/docs/LAB-4.md) as is (with this blockquote) but on VSC with the preview.

You can copy/paste this page as is to create your version. Then you will need to add markdown markup to proper format the output on the preview.

The source code is [here](https://gitlab.com/ChristopheLarsonneur/markdown-demo/-/blob/main/docs/LAB-4.md?plain=1). Do open it only if you want to compare your version and your version.

Remember to finally sign your document with the inline html code.

[[_TOC_]]

![markdown](https://upload.wikimedia.org/wikipedia/commons/thumb/5/51/Octicons-markdown.svg/240px-Octicons-markdown.svg.png)

Markdown is a great way to write technical document **readable in text mode**. It is easier and faster than writing a word processing application (like microsoft word)

We can generate it in different format.

Few examples:

- PDF : [Online md2pdf](https://md2pdf.netlify.app/), [pandoc][pandoc]

- HTML : [Online converter in HTML](https://markdowntohtml.com/), [Linux markdown](https://linuxhint.com/convert-markdown-files-linux/), [pandoc][pandoc]

- MSWord : [Online converter to MSWord](https://cloudconvert.com/md-to-docx), [pandoc][pandoc]

[pandoc]:https://pandoc.org/demos.html#examples

## One detailled option - Markdown on Linux with Pandoc

On Linux, you can do any kind of conversion from markdown format to HTML, PDF, etc, thanks to pandoc.

### Install on Fedora

Installation:

<details>
<summary>my summary</summary>

```bash
$ sudo dnf -y install pandoc
$
```

</details>

### Converting

Running The converter:

```bash
$ pandoc README.md --pdf-engine=xelatex -o example13.pdf
$
```

Thank you
